function onCvLoaded() {
    cv.onRuntimeInitialized = onReady;
}

tf.loadLayersModel('model/model.json').then(function (model) {
    window.model = model;
});

const waitingMsgDOM = document.getElementById('waitingMsg');
const playBtn = document.getElementById('playBtn');
const playerMoveDOM = document.getElementById('playerMove');
const playerMoveImgDOM = document.getElementById('playerMoveImg');
const botMoveDOM = document.getElementById('botMove');
const botMoveImgDOM = document.getElementById('botMoveImg');
const playerScoreDOM = document.getElementById('playerScore');
const botScoreDOM = document.getElementById('botScore');
const countdown = document.getElementById('countdown');
// const randomAgent = document.getElementById('random');
const video = document.getElementById('video');

let playerScore = 0;
let botScore = 0;

let playerLastMove = 'Rock';
let botLastMove = 'Paper';
let lastScore = 1;  // -1: bot loose, 0: draw, 1: bot win

const indexToLabel = ['Rock', 'Paper', 'Scissors'];
const width = 640;
const height = 480;
const cropX0 = 50;
const cropX1 = 350;
const cropY0 = 100;
const cropY1 = 400;
const cropDim = 300;
const imgDim = 150;
const FPS = 30;
let stream;

function onReady() {
    let src;
    let dst;
    let dstCrop;

    const rect = new cv.Rect(cropY0, cropX0, cropDim, cropDim);
    const dsize = new cv.Size(imgDim, imgDim);

    const cap = new cv.VideoCapture(video);

    // webcam
    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
        .then(stream => {
            waitingMsgDOM.hidden = true;

            video.srcObject = stream;
            video.play();
            streaming = true;
            src = new cv.Mat(height, width, cv.CV_8UC4);
            dst = new cv.Mat(height, width, cv.CV_8UC4);
            dstCrop = new cv.Mat(cropDim, cropDim, cv.CV_8UC1);
            setTimeout(processVideo, 0)
        })
        .catch(err => console.log(`An error occurred: ${err}`));

    // stream webcam
    function processVideo() {
        const begin = Date.now();
        cap.read(dst)
        // white rectangle draw. ROI
        cv.rectangle(dst, new cv.Point(cropY0, cropX0), new cv.Point(cropY1, cropX1), [255, 255, 255, 255], 2);
        cv.imshow('canvasOutput', dst);
        const delay = 1000 / FPS - (Date.now() - begin);
        setTimeout(processVideo, delay);
    }

    playBtn.addEventListener('click', () => {
        try {
            play();
        } catch (error) {
            console.log('Oops:', error);
        }
    });

    // actual play function. where everything lies
    function play(counter = 3) {
        countdown.textContent = counter;
        if (counter > 0) {
            setTimeout(play, 800, counter - 1);
            return;
        }

        getPrediction()
            .then(playerMove => {
                const botMove = makeMove(playerMove);

                updateScore(playerMove, botMove);
                updateUI();
            });
    }

    // prediction over frame from camera. RPS?
    function getPrediction() {
        // getFrame
        cap.read(src);

        // crop down to 300x300
        dstCrop = src.roi(rect);

        // process img: B&W, norm
        cv.cvtColor(dstCrop, dstCrop, cv.COLOR_RGBA2GRAY);
        // cv.normalize(dstCrop, dstCrop, alpha = 0, beta = 1, norm_type = cv.NORM_MINMAX, dtype = cv.CV_32F);
        cv.equalizeHist(dstCrop, dstCrop);

        // scale down to 150x150
        cv.resize(dstCrop, dstCrop, dsize, 0, 0, cv.INTER_AREA);

        // display cropped img
        cv.imshow('canvasOutputCrop', dstCrop);


        // predict
        let canvasOutputCrop = document.getElementById('canvasOutputCrop');

        // convert to tensor
        const tensor = tf.browser.fromPixels(canvasOutputCrop, 1)
            .resizeNearestNeighbor([imgDim, imgDim])
            .toFloat().expandDims();

        // get prediction
        const pred = window.model.predict(tensor);

        return pred.array()
            .then(scores => new Promise((resolve, reject) => {
                scores = scores[0];
                predicted = scores.indexOf(Math.max(...scores));
                resolve(indexToLabel[predicted]);
            }));
    }

    // bot player's move
    function makeMove(playerMove) {
        const agent = getAgent();

        switch (agent) {
            case 'Random':
                return makeRandomMove();
            case 'Cheater':
                return makeCheaterMove(playerMove);
            case 'Smart':
                return makeSmartMove(playerMove);
        }
    }

    function makeRandomMove() {
        return indexToLabel[Math.floor(Math.random() * 3)];
    }

    // janken robot
    function makeCheaterMove(playerMove) {
        return getOppositeMove(playerMove);
    }

    // win-stay, lose-shift
    function makeSmartMove() {
        if (lastScore == 1) {
            // player lost
            return playerLastMove;
        } else if (lastScore == -1) {
            // player won
            return getOppositeMove(playerLastMove);
        } else {
            // draw
            return getOppositeMove(getOppositeMove(playerLastMove));
        }
    }

    // returns the winning move over 'move'
    function getOppositeMove(move) {
        switch (move) {
            case 'Rock':
                return 'Paper';
            case 'Paper':
                return 'Scissors';
            case 'Scissors':
                return 'Rock';
        }
    }

    // updates scores in 'backend'
    function updateScore(playerMove, botMove) {
        // current scores
        const [currentPlayerScore, currentBotScore] = whoWon(playerMove, botMove);
        playerScore += currentPlayerScore;
        botScore += currentBotScore;

        // update lastScore
        if (currentPlayerScore == 0) {
            if (currentBotScore == 0) {
                // draw
                lastScore = 0;
            } else {
                // bot won
                lastScore = 1;
            }
        } else {
            // bot lost
            lastScore = -1;
        }

        // update lastMoves
        playerLastMove = playerMove;
        botLastMove = botMove;
    }

    // returns scores of current match. 1 for winner, 0 for loser
    function whoWon(playerMove, botMove) {
        if (playerMove == botMove) {
            // draw
            return [0, 0];
        }

        if (playerMove == 'Rock') {
            if (botMove == 'Paper') {
                // rock - paper
                return [0, 1];
            }
            // rock - Scissors
            return [1, 0];
        }

        if (playerMove == 'Paper') {
            if (botMove == 'Scissors') {
                // paper - Scissors
                return [0, 1];
            }
            // paper - rock
            return [1, 0];
        }

        if (botMove == 'Rock') {
            //Scissors - rock
            return [0, 1];
        }

        // Scissors - paper
        return [1, 0];
    }

    // update scores and moves on UI
    function updateUI() {
        playerMoveDOM.textContent = playerLastMove;
        botMoveDOM.textContent = botLastMove;

        // playerMoveImgDOM.src = `${playerLastMove.toLowerCase()}.png`;
        // playerMoveImgDOM.alt = playerLastMove;
        // playerMoveImgDOM.hidden = false;

        // botMoveImgDOM.src = `${botLastMove.toLowerCase()}.png`;
        // botMoveImgDOM.alt = botLastMove;
        // botMoveImgDOM.hidden = false;

        playerScoreDOM.textContent = playerScore;
        botScoreDOM.textContent = botScore;
    }

}

function getAgent() {
    const agents = document.querySelectorAll('input[name="agent"]');
    for (let ag of agents) {
        if (ag.checked) {
            return ag.value;
        }
    }
}