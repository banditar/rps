function onCvLoaded() {
    cv.onRuntimeInitialized = onReady;
}

tf.loadLayersModel('model/model.json').then(function (model) {
    window.model = model;
});

const waitingMsgDOM = document.getElementById('waitingMsg');
const playerMoveDOM = document.getElementById('playerMove');
const playerMoveImgDOM = document.getElementById('playerMoveImg');
const video = document.getElementById('video');

const indexToLabel = ['Rock', 'Paper', 'Scissors'];
const width = 640;
const height = 480;
const cropX0 = 50;
const cropX1 = 350;
const cropY0 = 100;
const cropY1 = 400;
const cropDim = 300;
const imgDim = 150;
const FPS = 30;
let stream;

function onReady() {
    let src;
    let dst;
    let dstCrop;

    const rect = new cv.Rect(cropY0, cropX0, cropDim, cropDim);
    const dsize = new cv.Size(imgDim, imgDim);

    const cap = new cv.VideoCapture(video);

    // webcam
    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
        .then(stream => {
            waitingMsgDOM.hidden = true;

            video.srcObject = stream;
            video.play();
            streaming = true;
            src = new cv.Mat(height, width, cv.CV_8UC4);
            dst = new cv.Mat(height, width, cv.CV_8UC4);
            dstCrop = new cv.Mat(cropDim, cropDim, cv.CV_8UC1);
            setTimeout(processVideo, 0)
        })
        .catch(err => console.log(`An error occurred: ${err}`));

    // stream webcam
    function processVideo() {
        const begin = Date.now();
        cap.read(dst)
        // white rectangle draw. ROI
        cv.rectangle(dst, new cv.Point(cropY0, cropX0), new cv.Point(cropY1, cropX1), [255, 255, 255, 255], 2);
        cv.imshow('canvasOutput', dst);
        const delay = 1000 / FPS - (Date.now() - begin);
        try {
            predictor();
        } catch (error) {
            console.log('Oops:', error);
        }
        setTimeout(processVideo, delay);
    }

    //
    function predictor() {
        getPrediction()
            .then(playerMove => {
                updateUI(playerMove);
            });
    }

    // prediction over frame from camera. RPS?
    function getPrediction() {
        // getFrame
        cap.read(src);

        // crop down to 300x300
        dstCrop = src.roi(rect);

        // process img: B&W, norm
        cv.cvtColor(dstCrop, dstCrop, cv.COLOR_RGBA2GRAY);
        cv.normalize(dstCrop, dstCrop, alpha = 0, beta = 1, norm_type = cv.NORM_MINMAX, dtype = cv.CV_32F);
        // cv.equalizeHist(dstCrop, dstCrop);

        // scale down to 150x150
        cv.resize(dstCrop, dstCrop, dsize, 0, 0, cv.INTER_AREA);

        // display cropped img
        cv.imshow('canvasOutputCrop', dstCrop);


        // predict
        let canvasOutputCrop = document.getElementById('canvasOutputCrop');

        // convert to tensor
        const tensor = tf.browser.fromPixels(canvasOutputCrop, 1)
            .resizeNearestNeighbor([imgDim, imgDim])
            .toFloat().expandDims();

        // get prediction
        const pred = window.model.predict(tensor);

        return pred.array()
            .then(scores => new Promise((resolve, _reject) => {
                scores = scores[0];
                predicted = scores.indexOf(Math.max(...scores));
                resolve(indexToLabel[predicted]);
            }));
    }

    // update scores and moves on UI
    function updateUI(playerMove) {
        // playerMoveImgDOM.src = `${playerMove.toLowerCase()}.png`;
        // playerMoveImgDOM.alt = playerMove;
        // playerMoveImgDOM.hidden = false;

        playerMoveDOM.textContent = playerMove;
    }

}