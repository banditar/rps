from flask import Flask, render_template

app = Flask(__name__,
            static_url_path='',
            static_folder='web/static',
            template_folder='web/templates')


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/predictor")
def predictor():
    return render_template("predictor.html")


@app.route("/howto")
def howto():
    return render_template("howto.html")


@app.errorhandler(404)
def handle_404(e):
    # handle all other routes here
    return render_template("error.html")


if __name__ == "__main__":
    app.run()
